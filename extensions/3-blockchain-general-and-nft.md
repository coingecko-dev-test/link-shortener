## General Blockchain
1. Bob owns a brand new luxurious apartment in Manhattan, New York. Can he use his apartment as collateral for a loan on https://compound.finance/ . If yes, explain how. If no, explain why not?
2. Given that 1 ETH = 1000 USDC. You contributed as a liquidity provider to a Uniswap V2 ETH/USDC pool of 5 ETH and 5000 USDC. The combined value is $10,000. Weeks later, the price of 1 ETH = 1500 USDC. Explain what happens to the funds you contributed to the liquidity provider pool?

## Exploring NFT (Non Fungible Token) using a block explorer

Let’s get additional information about Bored Ape Yacht Club (BAYC), an NFT (Non Fungible Token) project on the Ethereum blockchain.

The BAYC contract address is 0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d

Hint: Use Etherscan, an Ethereum block explorer to obtain the information.

1.  What is the metadata for NFT serial #3000 for BAYC? (Hint: The metadata is a json object explaining the attributes of the NFT)  
2.  How many BAYC NFTs are minted in total?  
3.  How many BAYC NFTs does the account 0x49c73c9d361c04769a452e85d343b41ac38e0ee4 hold?  
4.  Who is the owner of NFT serial #3000 for BAYC?  
5.  Is this metadata stored on IPFS or a Cloud Storage? What are the differences, pros and cons?


## Scoring Guide

Submissions will be evaluated based on the following criteria:

* For General question, the ability to make elaborated answer, knowing the ins/outs of how the protocol works.
* For Exploring NFT, use screenshots to support your answer when using the block explorer. Bonus points for using RPC API to obtain the answers.
