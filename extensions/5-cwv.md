# Extension 5: Core Web Vitals

Refer to [5-cwv.html](./5-cwv.html)

## Specifications

The web page provided (https://coingecko-dev-test.gitlab.io/link-shortener/) has a few major issues that may affect its core web vital score. You're tasked to improve it.

### Submission Guide

1. List the issues you can find. Briefly describe your approach.
1. Copy [5-cwv.html](./5-cwv.html) and apply your changes.
1. Submit both the list of issues and updated page.


Hint: There are at least 5 issues that can be found in this challenge
