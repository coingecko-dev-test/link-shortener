## Do not proceed 

This repository contains additional question sets. 

You may be asked to attempt some sets depending on the Level or Team you are interviewing for. Otherwise, you are **not required to attempt** this in a general assessment.
